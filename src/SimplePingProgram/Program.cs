﻿namespace SimplePingProgram
{
    using System.Net;
    using System.Net.Sockets;

    public static class Program
    {
        static void Main(string[] args)
        {
            var ipAddress = args[0];
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Raw, ProtocolType.Icmp);
            var ipEndPoint = new IPEndPoint(IPAddress.Parse(ipAddress), 0);

            socket.SendTo(new byte[0], ipEndPoint);
        }
    }
}
